# Introduction
We are in the business of online Scrabble.
One of the things we let our users do is define custom scoring tables.
For example, in regular English Scrabble,
the letter "A" has a value of 1 point,
but our system lets each user customize that value
(as well as other letter values).

Of course, this means storing a scoring table for each user in our database,
and we've decided that a database migration is in order...

# Scenario
Jay (your teammate) has gone on vacation.
He was tasked with migrating our legacy system's scoring tables
as described in this
[ETL exercise](https://exercism.org/tracks/typescript/exercises/etl).
Unfortunately, he didn't finish the job, and has passed his half completed
work on to you.

## Part 1 – Fixing Failing Tests
While looking at the old data, Jay noticed that some users have saved invalid
scoring tables.
Jay decided that the function responsible for migrating the data should
throw an exception if it encounters an invalid table.
He wrote a test for this, but the test is failing!

Have a look at Jay's code:
- Can we fix the failing test?
- Are there any other edge cases we can address?
- Can we make any other improvements to Jay's code?

## Part 2 – Implementing `score()`
If time permits,
implement and test a function that scores a given word when provided with a
scoring table (in the new format).
No worries if we don't get this far,
we can always tell Jay to do this when he gets back!

## Running Locally

Clone the project:

```shell
git clone git@gitlab.com:ninjacat-public/technical-interview-exercises/php-etl-exercise.git
```

Run tests:

```shell
./gradlew test
```