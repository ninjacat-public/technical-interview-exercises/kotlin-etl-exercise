typealias OldScoringTable = Map<Number, List<String>>
typealias NewScoringTable = Map<String, Number>

fun transform(oldTable: OldScoringTable): NewScoringTable {
    val newTable = mutableMapOf<String, Number>()

    oldTable.entries.forEach { (score, letters) ->
        letters.forEach {
            newTable[it.lowercase()] = score
        }
    }

    return newTable
}